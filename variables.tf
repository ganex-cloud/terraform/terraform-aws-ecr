variable "name" {
  description = "Repository name."
}

variable "max_images_retained" {
  description = "The max number of images to keep in the repository before expiring the oldest"
  default     = 10
}

variable "tags" {
  description = "A map of tags (key-value pairs) passed to resources."
  type        = "map"
  default     = {}
}

variable "role_arn" {
  description = "Role ARN that are trusted to pull and push to this repostitory"
  type        = "list"
  default     = []
}
